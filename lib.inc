section .text

%define EXITCODE 60
%define NEWLINE '/n'
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, EXITCODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .iterate:
        cmp byte[rax + rdi], 0
        je .return
        add rax, 1
        jmp .iterate
    .return:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp
    pop rdi
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, NEWLINE
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r13, rdi
    mov r14, 10
    mov r15, 1
    sub rsp, 1
    mov byte[rsp], 0
    .iterate:
        xor rdx, rdx
        mov rax, r13
        div r14
        mov r13, rax
	add rdx, 48
	sub rsp, 1
	mov [rsp], dl
	add r15, 1
	test r13, r13
	jnz .iterate
	mov rdi, rsp
	call print_string
	add rsp, r15   
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
%define MINUS 45
print_int:
    xor rax, rax
    test rdi, rdi
    jns .print
    neg rdi
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    .print:
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .iterate:
	mov r10b, byte[rcx + rdi]
	mov r11b, byte[rcx + rsi]
	cmp r10b, r11b
	jnz .different
	cmp r10b, 0
	jz .equal
	add rcx, 1
	jmp .iterate
    .different:
	mov rax, 0
	ret
    .equal:
	mov rax, 1
        ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    sub rsp, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .null
    mov al, [rsp]
    .null:
	add rsp, 1
    ret
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

%define SPACE ' '
%define TAB '	'
read_word:
    xor rax, rax
    xor rcx, rcx
    .iterate:
	push rsi
	push rdi
	push rcx
	call read_char
	pop rcx
	pop rdi
	pop rsi
	cmp rax, 0
	jz .return
	cmp rax, TAB
	jz .empty
	cmp rax, SPACE
	jz .empty
	cmp rax, NEWLINE
	jz .empty
	mov [rcx + rdi], rax
	add rcx, 1
	cmp rcx, rsi
	jnl .unsuitable
	jmp .iterate
    .empty:
	cmp rcx, 0
	jz .iterate
	jmp .return
    .unsuitable:
	xor rax, rax
	ret
    .return:
	xor rax, rax
	mov [rcx + rdi], rax
	mov rdx, rcx
	mov rax, rdi
    ret 
	
	
	
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
%define ZERO '0'
parse_uint:
    xor rax, rax
    xor r11, r11
    xor r10, r10
    xor r9, r9
    .iterate:
	mov r10b, [rdi]
	sub r10b, ZERO
	cmp r10b, 0
	jnge .return
	cmp r10b, TAB
	jnle .return
	imul r9, 10
	add r9, r10
	add r11, 1
	add rdi, 1
	jmp .iterate
    .return:
	mov rax, r9
	mov rdx, r11
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], MINUS
    jz .minus
    jmp parse_uint
    .minus:
	add rdi, 1
	call parse_uint
	neg rax
	add rdx, 1
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdx
    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi
    pop rdx
    cmp rax, rdx
    jnbe .unsuitable
    mov rcx, 0
    .iterate:
	mov r15b, byte[rcx + rdi]
	mov byte[rcx + rsi], r15b
	cmp rax, rcx
	jz .suitable
	add rcx, 1
	jmp .iterate
    .suitable:
	ret
    .unsuitable:
	mov rax, 0
    ret
